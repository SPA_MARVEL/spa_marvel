var PersistenciaLocalHome = {
	
	registrarDatos: function(_param){
		if ( !UtilitariasUI.esObjetoInvalido(typeof(Storage))) {
            localStorage.setItem(_param._sm_nombre,_param._sm_data);
        } 

	},
	
	actualizarDatos: function(_param){
		if ( !UtilitariasUI.esObjetoInvalido(typeof(Storage))) {
            localStorage.setItem(_param._sm_nombre,_param._sm_data);
        } 

	},
	
	consultarDatos: function(_param){
		if ( !UtilitariasUI.esObjetoInvalido(typeof(Storage))) {
            return localStorage.getItem(_param._sm_nombre); 
        } 
		return null;
	},
	
	eliminarDatos: function(_param){
		if ( !UtilitariasUI.esObjetoInvalido(typeof(Storage))) {
            return localStorage.removeItem(_param._sm_nombre); 
        } 
		return null;
	}
	
}