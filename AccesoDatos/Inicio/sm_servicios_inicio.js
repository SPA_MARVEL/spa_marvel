var ServiciosHome = {
	
	Buscar : function (_param,_vista){
        $.ajax({
             type: _param._sm_metodo
           , url: _param._sm_url
           , data: _param._sm_data
           , ContentType: _param._sm_tipodecontenido
           , beforeSend: function (sm_response, sm_requestOptions) {
               _vista.desplegarBarraDeProgreso();
           }
           , success: function (sm_data,sm_status,sm_response) {
               _vista.cerrarBarraDeProgreso();
           }
           , error: function (sm_response, sm_requestOptions, thrownError) {
			   var _param = {_sm_code : sm_response.status };
               _vista.cerrarBarraDeProgreso();
			   _vista.desplegarMensajeDeError();
           }
           , complete: function (sm_response,sm_status) {
			   switch (sm_response.status){
				   case EstatusPeticiones.correcto.Codigo:
			          _param._sm_accionpeticionexitosa(sm_response);
				   break;
			   }
           }
        });

    },
	

}
