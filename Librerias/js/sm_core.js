
var UtilitariasUI = {
	
	esObjetoInvalido: function(_param) {
          return _param != null && _param != undefined && _param != "undefined" && _param != "null" && _param != "" ? false : true;
    },
	
	esRespuestaDelServidorConDatos : function(_sm_respuesta){
	     if (!this.esObjetoInvalido(_sm_respuesta)){
			if (_sm_respuesta.responseJSON.data.results.length > 0){
				return true
			} 
		 }
		return false;
	},
	
	ajustarTextos: function(_param){
		
		var _resultados = null;
		var _longitudTexto;
		
		if (!this.esObjetoInvalido(_param.sm_texto)){
			if (_param.sm_texto.length > _param.sm_longitudTexto){
			  _resultados = _param.sm_texto.substring(0,_param.sm_longitudTexto) + Simbolos.finTexto;
			}
			else{
			   _resultados = _param.sm_texto;
			}
		}
		return _resultados;
	},
		
	desplegarMensajeDeError: function(_param){
		 var _sm_obj_modal = new ModalUI();
	 
		$(Simbolos.idJquery + _sm_obj_modal._sm_mdl_botonesdetalle_id).hide();
	    $(Simbolos.idJquery + _sm_obj_modal._sm_mdl_botonesinfo_id).show();
	    $(Simbolos.idJquery + _sm_obj_modal._sm_mdl_modal_id).modal("toggle");
	    $(Simbolos.idJquery + _sm_obj_modal._sm_img_id).attr("src", "../../images/sm_icono_errorrequest_1.png" );
		$(Simbolos.idJquery + _sm_obj_modal._sm_pnl_txt_id +" p").text(Mensajes._sm_errorgeneral_texto);
	    $(Simbolos.idJquery + _sm_obj_modal._sm_pnl_tituloprincipal_id + " p").text(Mensajes._sm_errorgeneral_titulo+ " (" +_param._sm_code + ") ");			
	},
	
	desplegarMensajeDatosNoEncontrados: function(_param){
		 var _sm_obj_modal = new ModalUI();
	 
		 $(Simbolos.idJquery + _sm_obj_modal._sm_mdl_botonesdetalle_id).hide();
	    $(Simbolos.idJquery + _sm_obj_modal._sm_mdl_botonesinfo_id).show();
	    $(Simbolos.idJquery + _sm_obj_modal._sm_mdl_modal_id).modal("toggle");
	    $(Simbolos.idJquery + _sm_obj_modal._sm_img_id).attr("src", "../../images/sm_icono_datosnoencontrados_1.jpg" );
		$(Simbolos.idJquery + _sm_obj_modal._sm_pnl_txt_id +" p").text(Mensajes._sm_datosnoencontrados_texto);
	    $(Simbolos.idJquery + _sm_obj_modal._sm_pnl_tituloprincipal_id + " p").text(Mensajes._sm_datosnoencontrados_titulo);		
	},
	
	obtenerIdDesdeUrl: function(_sm_url){
		    var _sm_comicId=null; 
			if (!this.esObjetoInvalido(_sm_url)){
                _sm_comicId = _sm_url.substring((_sm_url.lastIndexOf("/")+1),(_sm_url.length));             
            }
            return  _sm_comicId;      
	},
	
	consultarPaginas : function(_param){
		var _sm_total_paginacion = 1;
		
		if (parseInt(_param._sm_total) > parseInt(Valores._sm_limitePersonajes)){
			    _sm_total_paginacion = parseInt(_param._sm_total/Valores._sm_limitePersonajes);
				if (parseInt(_param._sm_total%Valores._sm_limitePersonajes) > 0){
					_sm_total_paginacion += 1;
				}
			}
			
	    return _sm_total_paginacion;
	}
	
}

