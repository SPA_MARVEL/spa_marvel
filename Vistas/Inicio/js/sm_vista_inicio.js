var _sm_obj_inicio;

$(document).unbind('ready').ready(function () {
	
	_sm_obj_inicio = new VistaHomeUI();
	_sm_obj_inicio.iniciarVista();
	
	 var left = $('#left').tabSlideOut({
      tabLocation: 'left',
      clickScreenToClose: false,
      offset: '300px',
      offsetReverse: true, // offset from bottom, not top
      // handlers: enable and disable buttons when sliding open or closed
      onOpen: function(){
          $('#open').prop('disabled',true);
          $('#close').prop('disabled',false);
      },
      onClose: function(){
          $('#open').prop('disabled',false);
          $('#close').prop('disabled',true);
      }      
    });
    
    $('#sm_pnl_favoritos_pestana').tabSlideOut({
      tabLocation: 'right',
      offsetReverse: true,
      handleOffsetReverse: true
    });
    
    $('#top').tabSlideOut({
      tabLocation: 'top',
      action: 'hover',
      handleOffsetReverse: true,
      offsetReverse: true,
      onLoadSlideOut: true
    });
    
    $('#bottom').tabSlideOut({
      tabLocation: 'bottom',  
      offset: '40px',
      otherOffset: '40px'
    });
    
    /* programmatically drive the left tab */
  //  $('button').click(function(event){
  //      left.tabSlideOut( /*command*/$(event.target).attr('id') );
   // });
    
    /* register event handler */
    $(document).on('slideouttabopen slideouttabclose slideouttabbounce',function(event){
        var text = $(event.target).attr('id')+': '+event.type;
        $('#events').append(text+"\n");
    });
	
		$('#sm_div_cuerpoBarraLateral').elastislide();

});


$(window).resize(function() {
   _sm_obj_inicio.configurarResolucionDeVentana();
   _sm_obj_inicio.configurarResolucionBarraDeProgreso();
});



function VistaHomeUI(){
	this._sm_obj_cabecera;
	this._sm_obj_barraLateralDesplegable;
	this._sm_obj_botonBarraLateralDesplegable;
	this._sm_obj_cuerpoPrincipal;
	this._sm_obj_piePrincipal;
	this._sm_obj_buscador;
	this._sm_obj_paginador;
	this._sm_obj_modal;
	this._sm_obj_barra_progreso;
	this._sm_obj_inicio;
	this._sm_obj_logica;
	
	
	this.iniciarVista = function(){

		var _vista = this;
		
		this._sm_obj_cabecera = new Cabecera_IU();
		this._sm_obj_barraLateralDesplegable = new BarraLateralDesplegableUI();
	    this._sm_obj_botonBarraLateralDesplegable  = new BotonBarraLateralDesplegableUI();
	    this._sm_obj_cuerpoPrincipal = new CuerpoPrincipalUI();
		this._sm_obj_piePrincipal = new PieUI();
	    this._sm_obj_buscador = new BuscadorUI();
		this._sm_obj_paginador = new PaginadorUI();
	    this._sm_obj_modal = new ModalUI();
		this._sm_obj_barra_progreso = new BarraDeProgresoUI;
	    this._sm_obj_inicio = new VistaHomeUI();
	    this._sm_obj_logica = new LogicaHomeUI();
		
		$(Simbolos.idJquery + this._sm_obj_modal._sm_mdl_botonesdetalle_id).show();
		$(Simbolos.idJquery + this._sm_obj_modal._sm_mdl_botonesinfo_id).hide();
		
		
		$(Simbolos.idJquery + this._sm_obj_buscador._sm_btn_buscador_id).unbind("click").click(function() {
			_vista._sm_obj_buscador._sm_str_parametro_busqueda  = $(Simbolos.idJquery+_vista._sm_obj_buscador._sm_txt_buscador_id).val();
			_vista._sm_obj_logica.consultarPersonajes(_vista);
		});
		
		$(Simbolos.idJquery + this._sm_obj_modal._sm_btn_agregarFavorito_id).click(function() {
			
			var _sm_obj_comic = new Comic();
			_sm_obj_comic._sm_id = $(Simbolos.idJquery+_vista._sm_obj_modal._sm_hdn_comic_id).val();
			_sm_obj_comic._sm_img =  $(Simbolos.idJquery+_vista._sm_obj_modal._sm_img_id).attr("src");
			_sm_obj_comic._sm_desc =  $(Simbolos.idJquery+_vista._sm_obj_modal._sm_pnl_tituloprincipal_id + " p").text();
			_vista.desplegarComicEnFavoritos(_sm_obj_comic);
		     var _param = {_sm_nombre: "_sm_str_favoritos",_sm_data: _sm_obj_comic};
			_vista._sm_obj_logica.registrarFavorito(_param);
		});
		
		this._sm_obj_logica.consultarFavoritos(this,_param = {_sm_nombre: Valores._sm_localstorage_nombre});
		
		this.configurarResolucionDeVentana();
		this.configurarResolucionBarraDeProgreso();

	}
	
	this.desplegarPersonaje = function(_sm_response){
		
		var _paramDescripcion = ""
		var _sm_obj_vistadetallecompletopersonajeHTML = new VistaDetalleCompletoPersonajeUI();
		var i = 0;
		var _sm_str_vistadetallecompletopersonajeHTML = "";
		var _sm_str_nombre_personaje="";
		
		if (!UtilitariasUI.esObjetoInvalido(_sm_response)){
			
			
			
			
				    _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenedor_principal;
					_sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_titulo_principal;
					
			
					
					
                    _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_response.responseJSON.data.results[i].name.toUpperCase();
					_sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_titulo_principal_fin;
	                _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML  + _sm_obj_vistadetallecompletopersonajeHTML._sm_descripcion_principal;
	                
	                _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + 
	                _sm_obj_vistadetallecompletopersonajeHTML._sm_imagen_principal;
	                _sm_str_vistadetallecompletopersonajeHTML = _sm_str_vistadetallecompletopersonajeHTML + '<img width="100%" height="100%" src="' + _sm_response.responseJSON.data.results[i].thumbnail.path + '/standard_incredible.jpg'  +  '">'; 
	                
	                 _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + 
	                _sm_obj_vistadetallecompletopersonajeHTML._sm_imagen_principal_fin;
	                
	                  _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + "<p>" + (UtilitariasUI.esObjetoInvalido(_sm_response.responseJSON.data.results[i].description) ? Mensajes._sm_contenidoNoDisponible :_sm_response.responseJSON.data.results[i].description) + "</p>";
	                
	                _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + 
	                _sm_obj_vistadetallecompletopersonajeHTML._sm_descripcion_principal_fin;
	                
	                
	               
	                _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_titulo_comics_principal + Mensajes._sm_tituloComics;
					_sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_titulo_comics_principal_fin;  
	                 
	                _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenedor_comics_principal; 
	                
	                 if (!UtilitariasUI.esObjetoInvalido(_sm_response.responseJSON.data.results[i].comics)){
						  if (_sm_response.responseJSON.data.results[i].comics.items.length > 0){
							    _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenedor_comics_linea_principal;
							    for (var j=0;j < _sm_response.responseJSON.data.results[i].comics.items.length;j++ ){
										
								     if (j!=0 && (j%2==0)){
										 _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenedor_comics_linea_principal_fin;
									     _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenedor_comics_linea_principal;
									}
									
									
                                    
                                     var _sm_comicId = UtilitariasUI.obtenerIdDesdeUrl(_sm_response.responseJSON.data.results[i].comics.items[j].resourceURI);
								    _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenido_comic_principal + ' onclick="' + '_sm_obj_inicio._sm_obj_logica.consultarComic(_sm_obj_inicio,{ _sm_comicId:' +  _sm_comicId +'  });" > ' ; 

					                _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_response.responseJSON.data.results[i].comics.items[j].name
					                _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenido_comic_principal_fin; 
								
							    }
		 	                    _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenedor_comics_linea_principal_fin;								
						 }
						 else{
							 
							  _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenedor_comics_linea_principal; 
							  _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenido_comic_na_principal; 
					          _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + Mensajes._sm_contenidoNoDisponible;
					          _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenido_comic_na_principal_fin;
							  _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenedor_comics_linea_principal_fin;							  
						 }
						 
					 }
	                
	                
	                
	                _sm_str_vistadetallecompletopersonajeHTML =  _sm_str_vistadetallecompletopersonajeHTML + _sm_obj_vistadetallecompletopersonajeHTML._sm_contenedor_comics_principal_fin; 


								
					$(Simbolos.idJquery + this._sm_obj_cuerpoPrincipal._sm_pnl_personajes_cont_id).append(_sm_str_vistadetallecompletopersonajeHTML);
					_sm_str_vistadetallecompletopersonajeHTML = "";
					
					
		
	    }
	}
	
	this.desplegarPersonajes = function(_sm_response){
		
		var _paramDescripcion = ""
		var _paramNombre = "";
		var _sm_obj_vistapreviapersonajeHTML = new VistaPreviaPersonajeUI();
		var _sm_str_vistapreviapersonajeHTML = "";
		
		if (!UtilitariasUI.esObjetoInvalido(_sm_response)){
				for (var i=0;i<_sm_response.responseJSON.data.results.length;i++){
				    _sm_str_vistapreviapersonajeHTML += _sm_obj_vistapreviapersonajeHTML._sm_contenedor_principal;
					_sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_titulo_principal;
					
					
					_paramNombre = {
						sm_texto: _sm_response.responseJSON.data.results[i].name.toUpperCase(),
						sm_longitudTexto: Valores.sm_longitud_texto_titulo_1
					}
                    _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML +  UtilitariasUI.ajustarTextos(_paramNombre);
					
					
					
					
					
					_sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_titulo_principal_fin;
	                _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_imagen_principal;
					_sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML + '<img width="100%" height="100%" src="' + _sm_response.responseJSON.data.results[i].thumbnail.path + '/standard_incredible.jpg'  +  '">'; 
					_sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_imagen_principal_fin;
					_sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML +  _sm_obj_vistapreviapersonajeHTML._sm_descripcion_principal;
                    _paramDescripcion = {sm_texto: UtilitariasUI.esObjetoInvalido(_sm_response.responseJSON.data.results[i].description) ? Mensajes._sm_contenidoNoDisponible :_sm_response.responseJSON.data.results[i].description, sm_longitudTexto: Valores.longitudTexto14px };
					_sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML + UtilitariasUI.ajustarTextos(_paramDescripcion);				 
					_sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_descripcion_principal_fin;
					_sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_boton_vermas_principal + ' onclick="' + '_sm_obj_inicio._sm_obj_logica.consultarPersonaje(_sm_obj_inicio,{ _sm_idpersonaje:' +  _sm_response.responseJSON.data.results[i].id  +'  }); ' + '"';
					_sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_boton_vermas_principal_fin;
					

					_sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_titulo_comics_principal + Mensajes._sm_tituloComics;
					_sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_titulo_comics_principal_fin; 
					_sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenedor_comics_principal; 
					
					 if (!UtilitariasUI.esObjetoInvalido(_sm_response.responseJSON.data.results[i].comics)){
						  if (_sm_response.responseJSON.data.results[i].comics.items.length > 0){
							    _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenedor_comics_linea_principal;
							    for (var j=0;j < _sm_response.responseJSON.data.results[i].comics.items.length;j++ ){
									if (j<Valores._sm_cantidadTitulosComics){
										
								     if (j!=0 && (j%2==0)){
										 _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenedor_comics_linea_principal_fin;
									     _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenedor_comics_linea_principal;
									}
										
									_paramDescripcion = {sm_texto: UtilitariasUI.esObjetoInvalido(_sm_response.responseJSON.data.results[i].comics.items[j].name) ? Mensajes._sm_contenidoNoDisponible :_sm_response.responseJSON.data.results[i].comics.items[j].name, sm_longitudTexto: Valores.longitudTexto9px };
									var _sm_comicId = UtilitariasUI.obtenerIdDesdeUrl(_sm_response.responseJSON.data.results[i].comics.items[j].resourceURI); 
								    _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenido_comic_principal + ' onclick="' + '_sm_obj_inicio._sm_obj_logica.consultarComic(_sm_obj_inicio,{ _sm_comicId:' +  _sm_comicId +'  });" > ' ; 
								    
								    
					                _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + UtilitariasUI.ajustarTextos(_paramDescripcion);
					                _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenido_comic_principal_fin; 
									}
									else{
										break;
									}
							    }
		 	                    _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenedor_comics_linea_principal_fin;								
						 }
						 else{
							  _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenedor_comics_linea_principal; 
							  _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenido_comic_na_principal; 
					          _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + Mensajes._sm_contenidoNoDisponible;
					          _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenido_comic_na_principal_fin; 
							  _sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenedor_comics_linea_principal_fin; 
						 }
						 
					 }
					
					_sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenedor_comics_principal_fin; 
					_sm_str_vistapreviapersonajeHTML =  _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_contenedor_principal_fin;
					
				
					$(Simbolos.idJquery + this._sm_obj_cuerpoPrincipal._sm_pnl_personajes_cont_id).append(_sm_str_vistapreviapersonajeHTML);
					_sm_str_vistapreviapersonajeHTML = "";
					
					
					}
					
		
	    }
	}
	
	this.desplegarPaginador = function(_sm_response){
        if (!UtilitariasUI.esObjetoInvalido(_sm_response)){

		
		   var _param = {_sm_total : _sm_response.responseJSON.data.total }
		   var _vista = this;
		   var _sm_total_paginacion = UtilitariasUI.consultarPaginas(_param);
		   this._sm_obj_paginador._sm_totalDatos = _sm_response.responseJSON.data.total;
		   this._sm_obj_paginador._sm_bool_esActivo = false;
			
	      $(Simbolos.idJquery + this._sm_obj_cuerpoPrincipal._sm_pnl_personajes_pag_id).html(this._sm_obj_paginador._sm_pnl_contenidoHTML);
		  $(Simbolos.idJquery + this._sm_obj_paginador._sm_pnl_paginador_id).twbsPagination({
			 totalPages: _sm_total_paginacion,
			 visiblePages: 5,
		     activeClass: 'sm_btn_paginador_activo',
			 paginationClass: 'sm_pnl_paginador_root',
		     pageClass: 'sm_btn_paginador_inactivo', 
		     firstClass: 'sm_btn_paginador_izq_inactivo',
		     prevClass: 'sm_btn_paginador_izq_prev_inactivo',
		     nextClass: 'sm_btn_paginador_der_next_inactivo',
		     lastClass: 'sm_btn_paginador_der_inactivo',
		     first: ' ',
		     prev: ' ',
             next: ' ',
		     last: ' ',
		     onPageClick: function (_sm_evento, _sm_numpagina) {

				 var _sm_offset = 0;
				 if (_vista._sm_obj_paginador._sm_bool_esActivo){
				     var _param = {_sm_numpagina : ((_sm_numpagina -1)*Valores._sm_offset)};
				     _vista._sm_obj_logica.consultarPersonajesPaginador(_vista,_param);
				 }
				 else{
					 _vista._sm_obj_paginador._sm_bool_esActivo = true;
				 }
             }
         });
		
		}
	}
	
	this.desplegarComic = function(_sm_response,_esComicValido){
		
		   var i = 0;
		   

			if (!UtilitariasUI.esObjetoInvalido(_sm_response)){
				
				if (!_esComicValido){
					$(Simbolos.idJquery + this._sm_obj_modal._sm_btn_agregarFavorito_id).hide();
					$(Simbolos.idJquery + this._sm_obj_modal._sm_btn_comprarComic_id).removeClass();
					$(Simbolos.idJquery + this._sm_obj_modal._sm_btn_comprarComic_id).addClass(this._sm_obj_modal._sm_btn_clasebotonesfull);
				}
				else{
                     $(Simbolos.idJquery + this._sm_obj_modal._sm_btn_agregarFavorito_id).show();
					 $(Simbolos.idJquery + this._sm_obj_modal._sm_btn_comprarComic_id).show();
					 $(Simbolos.idJquery + this._sm_obj_modal._sm_btn_agregarFavorito_id).removeClass();
					 $(Simbolos.idJquery + this._sm_obj_modal._sm_btn_comprarComic_id).removeClass();
					 $(Simbolos.idJquery + this._sm_obj_modal._sm_btn_agregarFavorito_id).addClass(this._sm_obj_modal._sm_btn_clasebotonesdefault);
					 $(Simbolos.idJquery + this._sm_obj_modal._sm_btn_comprarComic_id).addClass(this._sm_obj_modal._sm_btn_clasebotonesdefault);
				}

				$(Simbolos.idJquery + this._sm_obj_modal._sm_mdl_botonesdetalle_id).show();
	        	$(Simbolos.idJquery + this._sm_obj_modal._sm_mdl_botonesinfo_id).hide();				
	        	$(Simbolos.idJquery + this._sm_obj_modal._sm_mdl_modal_id).modal("toggle");
	        	$(Simbolos.idJquery + this._sm_obj_modal._sm_img_id).attr("src", _sm_response.responseJSON.data.results[i].thumbnail.path  + '/standard_incredible.jpg' );
				$(Simbolos.idJquery + this._sm_obj_modal._sm_pnl_txt_id +" p").text(UtilitariasUI.esObjetoInvalido(_sm_response.responseJSON.data.results[i].description)?Mensajes._sm_contenidoNoDisponible:_sm_response.responseJSON.data.results[i].description);
				$(Simbolos.idJquery + this._sm_obj_modal._sm_pnl_tituloprincipal_id + " p").text(_sm_response.responseJSON.data.results[i].title);				
				$(Simbolos.idJquery + this._sm_obj_modal._sm_hdn_comic_id).val(_sm_response.responseJSON.data.results[i].id);	

			
			}			
	}
	
	this.desplegarComicEnFavoritos = function(_param){
		
		  if (!UtilitariasUI.esObjetoInvalido(_param)){

		  var _sm_obj_vistapreviapersonajeHTML = new ComicFavoritoUI();
		  var _sm_str_vistapreviapersonajeHTML = "";
		  _sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_pnl_principalHTML;
		  _sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML + "favorito_"+_param._sm_id+ _sm_obj_vistapreviapersonajeHTML._sm_pnl_principal_headerHTML_fin;
 
		  _sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_pnl_img_eliminarHTML;
		  _sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML + "_sm_obj_inicio._sm_obj_logica.eliminarFavorito("+"{_sm_id:"+ _param._sm_id  +",_sm_nombre:'"+ Valores._sm_localstorage_nombre+"'})";
		  _sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_pnl_img_eliminarHTML_fin;
		  
		
		   _sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_img_principalHTML + _param._sm_img ;
		   _sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_img_principalHTML_fin;
		   
	
		    _sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_txt_descripcionHTML + _param._sm_desc;
		   _sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_txt_descripcionHTML_fin;
		   
		   _sm_str_vistapreviapersonajeHTML = _sm_str_vistapreviapersonajeHTML + _sm_obj_vistapreviapersonajeHTML._sm_pnl_principalHTML_fin;
		   
   
		   $(Simbolos.idJquery + this._sm_obj_barraLateralDesplegable._sm_barra_contenido_id).append(_sm_str_vistapreviapersonajeHTML);
		   
		   $(Simbolos.idJquery + this._sm_obj_modal._sm_mdl_modal_id).modal("hide");
       }
	}
	
	this.desplegarComicsEnFavoritos = function(_sm_favoritos){
		 if (!UtilitariasUI.esObjetoInvalido(_sm_favoritos)){
			 for (var i=0;i < _sm_favoritos.length;i++){
				 var aa = _sm_favoritos[i];
				 this.desplegarComicEnFavoritos(_sm_favoritos[i]);
			 }
		 }
	}
	
	this.eliminarFavorito = function(_param){
		if (!UtilitariasUI.esObjetoInvalido(_param)){
			
			$(Simbolos.idJquery + _param._sm_id + " ").remove(); 
		}
	}
		
    this.colocarPosicionScrollVista = function(_param){
		window.scrollTo(_param._sm_x,_param._sm_y);
	}
	
	this.borrarPersonajes = function(){
		$(Simbolos.idJquery + this._sm_obj_cuerpoPrincipal._sm_pnl_personajes_cont_id).html("");
		$(Simbolos.idJquery + this._sm_obj_cuerpoPrincipal._sm_pnl_personajes_pag_id).html("");
	}
	
	this.borrarContenidoPrincipal = function(){
		$(Simbolos.idJquery + this._sm_obj_cuerpoPrincipal._sm_pnl_personajes_cont_id).html("");
	
	}
	
	this.borrarPaginadorPrincipal = function(){
		$(Simbolos.idJquery + this._sm_obj_cuerpoPrincipal._sm_pnl_personajes_pag_id).html("");
	}
	
	this.configurarResolucionDeVentana = function(){
		
		var _sm_int_cabecera_largo = $(Simbolos.idJquery + this._sm_obj_cabecera._sm_pnl_cabecera_id).height();
		var _sm_int_cuerpo_largo = $(Simbolos.idJquery +this._sm_obj_cuerpoPrincipal._sm_pnl_personajes_id).height();
		var _sm_int_ventana_largo = window.innerHeight;
		var _sm_int_pie_largo = $(Simbolos.idJquery +this._sm_obj_piePrincipal._sm_pnl_pie_id).height();
		
		_sm_int_cuerpo_largo = (_sm_int_ventana_largo - _sm_int_cabecera_largo);
		 $(Simbolos.idJquery +this._sm_obj_cuerpoPrincipal._sm_pnl_personajes_id).css("min-height",_sm_int_cuerpo_largo);
		
	}
	
	this.configurarResolucionBarraDeProgreso = function(){
			
		var _sm_int_ventana_largo = window.innerHeight/2;

		 $(Simbolos.idJquery +this._sm_obj_barra_progreso._sm_pnl_contenido_barra_progreso_id).css("margin-top",_sm_int_ventana_largo);
		
	}
	
    this.desplegarBarraDeProgreso = function(){
		 $(Simbolos.idJquery+this._sm_obj_barra_progreso._sm_pnl_barra_progreso_id).fadeIn(  );
		 $(Simbolos.idJquery+this._sm_obj_barra_progreso._sm_background_barra_progreso_id).fadeIn( );
		 
				
	}
	
	this.cerrarBarraDeProgreso = function(){
		 $(Simbolos.idJquery+this._sm_obj_barra_progreso._sm_pnl_barra_progreso_id).fadeOut(  );
		 $(Simbolos.idJquery+this._sm_obj_barra_progreso._sm_background_barra_progreso_id).fadeOut( );
		 
	}
	
	
	
}