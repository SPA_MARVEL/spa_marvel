
function LogicaHomeUI(){
	
	this.consultarPersonaje = function(_vista, _param){
		
			var _sm_timestamp = Date.now();
			var _sm_param_hash = _sm_timestamp.toString() + Configuracion.sm_str_claveprivada +  Configuracion.sm_str_clavepublica ;
			var _sm_hash = $.md5(_sm_param_hash);
			var _sm_obj_personaje;
		
            var _param = {
				_sm_metodo : "GET",
				_sm_url : Configuracion.sm_str_nombrededominio + Configuracion.sm_str_root + "characters/"+ _param._sm_idpersonaje +  "?ts=" + _sm_timestamp + "&apikey=" + Configuracion.sm_str_clavepublica + "&hash=" + _sm_hash,
				_sm_data : {},
				_sm_tipodecontenido : "application/x-www-form-urlencoded; charset=UTF-8",
				_sm_accionpeticionexitosa : function(_sm_respuesta){
					if (UtilitariasUI.esRespuestaDelServidorConDatos(_sm_respuesta)){
						_sm_obj_personaje = this.obtenerPersonajeConFormato(_sm_respuesta);
						_vista.borrarPersonajes();
						_vista.desplegarPersonaje(_sm_obj_personaje);
					}
					else{
						_vista.borrarPersonaje();
						 UtilitariasUI.desplegarMensajeDatosNoEncontrados();
					}
					
					_vista.colocarPosicionScrollVista({_sm_x:0,_sm_y:0});
					
				}
			}
			
			ServiciosHome.Buscar(_param,_vista);
		
	},
	
	this.consultarPersonajes = function(_vista){
		
			var _sm_timestamp = Date.now();
			var _sm_param_hash = _sm_timestamp.toString() + Configuracion.sm_str_claveprivada +  Configuracion.sm_str_clavepublica ;
			var _sm_hash = $.md5(_sm_param_hash);
			var _sm_nombredelpersonaje = _vista._sm_obj_buscador._sm_str_parametro_busqueda;
			
            var _param = {
				_sm_metodo : "GET",
				_sm_url : Configuracion.sm_str_nombrededominio + Configuracion.sm_str_root + "characters?"+"ts=" + _sm_timestamp + (!UtilitariasUI.esObjetoInvalido(_sm_nombredelpersonaje) ? ("&nameStartsWith=" + _sm_nombredelpersonaje): "" )+ "&apikey=" + Configuracion.sm_str_clavepublica + "&hash=" + _sm_hash+"&limit="+Valores._sm_limitePersonajes,
				_sm_data : {},
				_sm_tipodecontenido : "application/x-www-form-urlencoded; charset=UTF-8",
				_sm_accionpeticionexitosa : function(_sm_respuesta){
					if (UtilitariasUI.esRespuestaDelServidorConDatos(_sm_respuesta)){
						_vista.borrarPersonajes();
						_vista.desplegarPersonajes(_sm_respuesta);
						_vista.desplegarPaginador(_sm_respuesta);
					}
					
					else{
						_vista.borrarPersonajes();
						UtilitariasUI.desplegarMensajeDatosNoEncontrados();
					}
					
					_vista.colocarPosicionScrollVista({_sm_x:0,_sm_y:0});
					
				}
			}
			
			ServiciosHome.Buscar(_param,_vista);
		
	},
	
	this.consultarPersonajesPaginador = function(_vista,_param){
		
			var _sm_timestamp = Date.now();
			var _sm_param_hash = _sm_timestamp.toString() + Configuracion.sm_str_claveprivada +  Configuracion.sm_str_clavepublica ;
			var _sm_hash = $.md5(_sm_param_hash);
			var _sm_nombredelpersonaje = _vista._sm_obj_buscador._sm_str_parametro_busqueda;
			var _sm_offset = _param._sm_numpagina;
			
            var _param = {
				_sm_metodo : "GET",
				_sm_url : Configuracion.sm_str_nombrededominio + Configuracion.sm_str_root + "characters?"+"ts=" + _sm_timestamp + (!UtilitariasUI.esObjetoInvalido(_sm_nombredelpersonaje) ? ("&nameStartsWith=" + _sm_nombredelpersonaje): "" )+ "&apikey=" + Configuracion.sm_str_clavepublica + "&hash=" + _sm_hash+"&limit="+Valores._sm_limitePersonajes+"&offset="+_sm_offset,
				_sm_data : {},
				_sm_tipodecontenido : "application/x-www-form-urlencoded; charset=UTF-8",
				_sm_accionpeticionexitosa : function(_sm_response){

					if (_sm_response.responseJSON.data.results.length > 0){
						_vista.borrarContenidoPrincipal();
						_vista.desplegarPersonajes(_sm_response);
					}
					
					else{
						_vista.borrarPersonajes();
						UtilitariasUI.desplegarMensajeDatosNoEncontrados();
					}
					
					_vista.colocarPosicionScrollVista({_sm_x:0,_sm_y:0});
					
				}
			}
			
			ServiciosHome.Buscar(_param, _vista);
		
	},
	
	
	this.consultarComic= function(_vista,_param){

			var _sm_timestamp = Date.now();
			var _sm_param_hash = _sm_timestamp.toString() + Configuracion.sm_str_claveprivada +  Configuracion.sm_str_clavepublica ;
			var _sm_hash = $.md5(_sm_param_hash);
			var _sm_obj_logica = this;
			var i = 0;
	
            var _param = {
				_sm_metodo : "GET",
				_sm_url : Configuracion.sm_str_nombrededominio + Configuracion.sm_str_root + "comics/"+_param._sm_comicId+"?ts=" + _sm_timestamp +"&apikey=" + Configuracion.sm_str_clavepublica + "&hash=" + _sm_hash,
				_sm_data : {},
				_sm_tipodecontenido : "application/x-www-form-urlencoded; charset=UTF-8",
				_sm_accionpeticionexitosa : function(_sm_response){

					if (_sm_response.responseJSON.data.results.length > 0){
						var _esComicValido = _sm_obj_logica.validarFavorito(_sm_response);
						_vista.desplegarComic(_sm_response, _esComicValido );
					}
					
					else{
						_vista.borrarPersonajes();
						UtilitariasUI.desplegarMensajeDatosNoEncontrados();
					}
					
				}
			}
			
			ServiciosHome.Buscar(_param,_vista);
		
	},
	

	
	this.registrarFavorito = function(_param){
		
		var _sm_obj_favoritos;
		var _sm_str_favoritos;

		if ( !UtilitariasUI.esObjetoInvalido(_param)) {
			
			_sm_str_favoritos = PersistenciaLocalHome.consultarDatos(_param);
			if ( !UtilitariasUI.esObjetoInvalido(_sm_str_favoritos)) {
				_sm_obj_favoritos = JSON.parse(_sm_str_favoritos);
				
					_sm_obj_favoritos._sm_comics.push(_param._sm_data);
					_param._sm_data =  JSON.stringify(_sm_obj_favoritos);
					PersistenciaLocalHome.registrarDatos(_param);
			}
			else{
				_sm_obj_favoritos = new Comics();
				_sm_obj_favoritos._sm_comics.push(_param._sm_data);
				_param._sm_data =  JSON.stringify(_sm_obj_favoritos);
				PersistenciaLocalHome.registrarDatos(_param);
			}
		}
	}
	
	
    this.consultarFavoritos = function(_vista,_param){
		var _sm_obj_favoritos;
		var _sm_str_favoritos;

		if ( !UtilitariasUI.esObjetoInvalido(_param)) {
			
			_sm_str_favoritos = PersistenciaLocalHome.consultarDatos(_param);
			if ( !UtilitariasUI.esObjetoInvalido(_sm_str_favoritos)) {
				_sm_obj_favoritos = JSON.parse(_sm_str_favoritos);
				_vista.desplegarComicsEnFavoritos(_sm_obj_favoritos._sm_comics);
			}
		}
		
		_vista.colocarPosicionScrollVista({_sm_x:0,_sm_y:0});
	}
	
	this.eliminarFavorito = function(_param){

		var _sm_obj_favoritos;
		var _sm_str_favoritos;
		var _sm_index = -1;

		if ( !UtilitariasUI.esObjetoInvalido(_param)) {
			
			_sm_str_favoritos = PersistenciaLocalHome.consultarDatos(_param);
			if ( !UtilitariasUI.esObjetoInvalido(_sm_str_favoritos)) {
				_sm_obj_favoritos = JSON.parse(_sm_str_favoritos);
				for (var i=0;i<_sm_obj_favoritos._sm_comics.length;i++){
					if (parseInt(_sm_obj_favoritos._sm_comics[i]._sm_id) === parseInt(_param._sm_id)){
						_sm_index = i;
						
						break;
					}
			    }
				
				if (_sm_index > -1){
					var _vista = new VistaHomeUI();
					_vista.eliminarFavorito({_sm_id: (Valores._sm_prefijo+ _sm_obj_favoritos._sm_comics[_sm_index]._sm_id)});
					_sm_obj_favoritos._sm_comics.splice(_sm_index,1);
					_param._sm_data =  JSON.stringify(_sm_obj_favoritos);
					PersistenciaLocalHome.actualizarDatos(_param);
				}
			}
		}
		
	}
	
	this.validarFavorito = function(_sm_response){
		var _sm_obj_favoritos;
		var _sm_str_favoritos;
		var _sm_resultados = true;
		var _param;
		var _sm_index = -1;
		var _sm_index = 0;

		if ( !UtilitariasUI.esObjetoInvalido(_sm_response)) {
			_param = {_sm_id:_sm_response.responseJSON.data.results[_sm_index].id, _sm_nombre: Valores._sm_localstorage_nombre}
			_sm_str_favoritos = PersistenciaLocalHome.consultarDatos(_param);
			if ( !UtilitariasUI.esObjetoInvalido(_sm_str_favoritos)) {
				_sm_obj_favoritos = JSON.parse(_sm_str_favoritos);
				for (var i=0;i<_sm_obj_favoritos._sm_comics.length;i++){
					if (parseInt(_sm_obj_favoritos._sm_comics[i]._sm_id) === parseInt(_param._sm_id)){
						_sm_resultados = false;
						break;
					}
			    }
			}
		}
		
		return _sm_resultados;
	}
	
	
	this.obtenerPersonajeConFormato = function(_sm_obj_respuesta){
		
		var _sm_obj_personaje = new Personaje();
		
		if ( !UtilitariasUI.esObjetoInvalido(_sm_obj_respuesta)) {
			 _sm_obj_personaje._sm_str_nombre = _sm_response.responseJSON.data.results[i].name.toUpperCase();
			 _sm_obj_personaje._sm_str_descripcion = _sm_response.responseJSON.data.results[i].description;
			 _sm_obj_personaje._sm_str_imagen_url =  _sm_response.responseJSON.data.results[i].thumbnail.path + '/standard_incredible.jpg';
			 _sm_obj_personaje._sm_col_historietas = _sm_response.responseJSON.data.results[i].comics;
		}
		
		return _sm_obj_personaje;
	}
	
}








