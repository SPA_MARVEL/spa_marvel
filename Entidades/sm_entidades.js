
function VistaPreviaPersonajeUI(){
	this._sm_contenedor_principal = '<div class="sm_pnl_personaje_previo">';
	this._sm_titulo_principal = '<h3 class="sm_txt_nombre_personaje">';
	this._sm_imagen_principal = '<div class="sm_pnl_imagen_personaje">';
	
	this._sm_descripcion_principal = '<div class="sm_txt_descripcion_personaje" ><p>';
	this._sm_boton_vermas_principal = '	<input class="sm_btn_ver_detalles_personaje"type="button" value="View More"';
	this._sm_titulo_comics_principal = '<h4 class="sm_txt_titulo_historietas_personajes">';
	this._sm_contenedor_comics_principal = '<div class="sm_pnl_characterprevie_comics">';
	this._sm_contenido_comic_principal = '<div class="sm_txt_nombre_historieta_personaje"><p><a';  
	this._sm_contenido_comic_na_principal = '<div class="sm_txt_nombre_historieta_personaje_na"><p>'
	this._sm_contenedor_comics_linea_principal = ' <div class="sm_pnl_nombre_historieta_personaje">';
	

	
	this._sm_contenedor_principal_fin = '</div>';
	this._sm_titulo_principal_fin = '</h3>';
	this._sm_imagen_principal_fin = '</div>';
	this._sm_descripcion_principal_fin = '</p></div>';
	this._sm_boton_vermas_principal_fin = '	>';
	this._sm_titulo_comics_principal_fin = '</h4>';
	this._sm_contenedor_comics_principal_fin = '</div>';
	this._sm_contenido_comic_principal_fin = '</a></p></div>';
	this._sm_contenido_comic_na_principal_fin = '</p></div>';
	this._sm_contenedor_comics_linea_principal_fin = ' </div>';
}

function VistaDetalleCompletoPersonajeUI(){
	this._sm_contenedor_principal = '<div class="sm_pnl_characterdetail">';
	this._sm_contenido_principal = '<div class="sm_pnl_characterdetail_cont">';
	this._sm_titulo_principal = '<h3 class="sm_txt_characterdetail_titu">';
	this._sm_imagen_principal = '<div class="sm_pnl_characterdetail_img">';
	
	this._sm_descripcion_principal = '<div class="sm_txt_characterdetail_desc" ><p>';
	this._sm_titulo_comics_principal = '<h4 class="sm_txt_characterdetail_titu_2">';
	this._sm_contenedor_comics_principal = '<div class="sm_pnl_characterdetail_comics">';
	this._sm_contenido_comic_principal = '<div class="sm_txt_characterdetail_comics"><p><a';
	
	
	this._sm_contenido_comic_na_principal = '<div class="sm_txt_characterdetail_comics_na"><p>'
	this._sm_contenedor_comics_linea_principal = ' <div class="sm_pnl_characterdetail_comics_linea">';
	
	this._sm_contenido_head_principal = '<h3>';
	
	this._sm_contenedor_principal_fin = '</div>';
	this._sm_contenido_principal_fin = '</div>';
	this._sm_titulo_principal_fin = '</h3>';
	this._sm_contenido_head_principal_fin = '</h3>';
	this._sm_imagen_principal_fin = '</div>';
	this._sm_descripcion_principal_fin = '</p></div>';
	this._sm_boton_vermas_principal_fin = '	>';
	this._sm_titulo_comics_principal_fin = '</h4>';
	this._sm_contenedor_comics_principal_fin = '</div>';
	this._sm_contenido_comic_principal_fin = '</a></p></div>';
	this._sm_contenido_comic_na_principal_fin = '</p></div>';
	this._sm_contenedor_comics_linea_principal_fin = ' </div>';
}

function Cabecera_IU(){
	this._sm_pnl_cabecera_id = "sm_pnl_cabecera_estatica";
}

function CuerpoPrincipalUI(){
	
   this._sm_pnl_cuerpo_principal_id = "sm_cuerpo_principal";	
   this._sm_main_static_id = "sm_main_static";
   this._sm_main_class = ".sm_main";
   this._sm_pnl_personajes_id = "sm_pnl_personajes";
   this._sm_pnl_personajes_cont_id = "sm_pnl_personajes_contenido";
   this._sm_pnl_personajes_pag_id = "sm_pnl_personajes_paginador";
   
}

function PieUI(){
	this._sm_pnl_pie_id = "sm_pie_principal";
}

function BuscadorUI(){
	this._sm_txt_buscador_id="sm_buscador";
	this._sm_btn_buscador_id = "sm_btn_buscador"; 
	this._sm_str_parametro_busqueda = "";
}


function BarraLateralDesplegableUI(){
	this._sm_barra_favoritos_id = "sm_pnl_favoritos_cuerpo";
	this._sm_barra_contenido_id="sm_pnl_favoritos_contenido";
	this._sm_px_ancho = "";
	this._sm_px_posicion = "";
}

function BotonBarraLateralDesplegableUI(){
	this._sm_boton_favoritos_id = "main";
	this._sm_px_ancho = "" ;
	this._sm_px_posicion = "";
}

function ComicFavoritoUI(){
   this._sm_pnl_principalHTML = '<div class="sm_pnl_fav_comic" id="';
   this._sm_pnl_principal_headerHTML_fin = '">';
   this._sm_pnl_principalHTML_fin = ' </div>';
   this._sm_link_eliminarHTML = '<a onclick="';
   this._sm_link_eliminar_headerHTML_fin = '">';
   this._sm_link_eliminarHTML_fin = '</a>';
   
   this._sm_img_principalHTML = '<div class="sm_pnl_img_comic"><img width="100%" height="273px"   src="';
   this._sm_img_principalHTML_fin = '"></div>';
   
   this._sm_txt_descripcionHTML = '<div class="sm_pnl_txt_comic"><p>"';
   this._sm_txt_descripcionHTML_fin = '</p></div>';
   
   this._sm_pnl_img_eliminarHTML = '<img class="sm_img_trash_comic" width="35px" height="35px" src="../../images/btn-delete.png" onclick="  '
   this._sm_pnl_img_eliminarHTML_fin = '">';
}

function BarraDeProgresoUI(){
	this._sm_mdl_barra_progreso_id = "sm_mdl_barra_progreso";
	this._sm_pnl_barra_progreso_id = "sm_pnl_barra_progreso";
	this._sm_background_barra_progreso_id = "sm_background_barra_progreso";
	this._sm_pnl_contenido_barra_progreso_id = "sm_pnl_contenido_barra_progreso";
	this._sm_pnl_animacion_id = "fountainG";
	
	
	
	
}

function ModalUI(){
	this._sm_mdl_modal_id="sm_mdl_personajes_historietas";
	this._sm_mdl_botonesdetalle_id = "_sm_mdl_botones_personajes_historietas"; 
	this._sm_mdl_botonesinfo_id = "_sm_mdl_botones_personajes_historietas_info";
	this._sm_pnl_img_id = "sm_pnl_detallecomic_img";
	this._sm_pnl_txt_id = "sm_pnl_detallecomic_txt";
	this._sm_pnl_tituloprincipal_id = "sm_pnl_personajes_titulo_historietas";
	this._sm_img_id="_sm_img_detallecomic";
	this._sm_mdl_errorinfoHTML = "<div class='_sm_pnl_tituloinfo'><p>";
	this._sm_mdl_errorinfoHTML_fin = "</p></div>";
	this._sm_btn_agregarFavorito_id = "_sm_btn_agregarFavorito";
	this._sm_btn_comprarComic_id = "_sm_btn_comprarComic";
	this._sm_hdn_comic_id="_sm_hdn_id";
	this._sm_btn_clasebotonesdefault = "btn btn-secondary col-xs-12 col-sm-12 col-lg-6 sm_btn_comicdetalle";
	this._sm_btn_clasebotonesfull = "btn btn-secondary col-xs-12 col-sm-12 col-lg-12 sm_btn_comicdetalle";
	
}

function PaginadorUI(){
	this._sm_pnl_paginador_id = "sm_pnl_paginador";
	this._sm_bool_esActivo = false;
	this._sm_totalDatos = 0;
	this._sm_pnl_contenidoHTML = '<div id="sm_pnl_paginador" class="sm_pnl_paginador"></div>';
}


function Personaje(){
	this._sm_str_nombre;
	this._sm_str_descripcion;
	this._sm_str_imagen_url;
	this._sm_col_historietas = new Array();
	
}

function Comic(){
	this._sm_id;
    this._sm_img="";
    this._sm_desc="";
}

function Comics(){
	this._sm_comics = new Array();
}


var Simbolos = {
	idJquery: '#',
	finTexto : '...'
}

var Valores = {
	anchoPixelesTipo1: '250px',
	anchoPixelesTipo2 : '0px',
	posicionPixelesTipo1 : "1200px",
	sm_longitud_texto_titulo_1: "17",
	longitudTexto14px: "85",
	longitudTexto9px: "40",
    _sm_cantidadTitulosComics : 4,
    _sm_limitePersonajes : 10,
    _sm_offset : 10,
    _sm_localstorage_nombre : "_sm_str_favoritos",
    _sm_prefijo: 'favorito_'	
}

var Mensajes = {
	_sm_tituloComics : "Related comics",
	_sm_contenidoNoDisponible : "Not Available.",
	_sm_barraProgresoBuscador : "Searching Characteres...",
	_sm_errorgeneral_titulo : "!..Ups..¡ An error has just occurred.",
	_sm_errorgeneral_texto : "Please contact your system administrator for more information.",
	_sm_datosnoencontrados_titulo : "Unsuccessful Search",
	_sm_datosnoencontrados_texto : "Please, try again."
}

var Configuracion = {
	sm_str_clavepublica : "7496aae53f158cc22c0447f8b592d6fa",
	sm_str_claveprivada : "aed3416c4e7cd8466a4bc9ac587ea585d672a253",
	sm_str_nombrededominio : "https://gateway.marvel.com:443",
	sm_str_root : "/v1/public/"
}


var EstatusPeticiones = {
	correcto : {Mensaje:"",Codigo:200},
	invalido : {Mensaje:"",Codigo:400},
	error : {Mensaje:"",Codigo:500},
	redireccion : {Mensaje:"",Codigo:300}
}